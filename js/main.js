

var App = {
    launch: function() {
        App.result();
    },
    result: function(){
        let pokemon = $("#pokeselect").val()
        let candy = $("#candyinput").val()
        let pokeAmount = $("#pokeinput").val()
        let evolveAmt = 0
        candy = parseInt(candy)
        pokeAmount = parseInt(pokeAmount)
        if (pokeAmount < 0 || candy < 0 || isNaN(pokeAmount) || isNaN(candy)){
            throw new window.onError();
        }
        while(pokeAmount > 0 && candy > 11){
            candy = candy - 12
            pokeAmount = pokeAmount - 1
            evolveAmt = evolveAmt + 1
            candy = candy + 2
        }
        let exp = evolveAmt * 500
        let r = "Can evolve " + evolveAmt + " " + pokemon + "s<br>"
        + candy + " candy left<br>"
        + pokeAmount + " " + pokemon + "s left<br>"
        + exp + " experience gained"
        $("#resultText").html(r)    //Here's the jQuery
        document.getElementById(resultText) = r     //Here's the Javascript, they both do the same thing
    }

};

window.onError = function() {
    var message = "INVALID INPUT";
    alert(message);
    return true;
  };